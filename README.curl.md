## Examples

### Prepare
NAME=foobar

### Get time-series data using dynamodb
curl -X GET "http://127.0.0.1:5000/api/v1/device/info?name=$NAME"
curl -X GET "http://127.0.0.1:5000/api/v1/device/info?name=$NAME" | jq .

### Get preset data using s3
curl -X GET "http://127.0.0.1:5000/api/v1/preset/get
curl -X GET "http://127.0.0.1:5000/api/v1/preset/get" | jq .