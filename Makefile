.PHONY: virtualenv clean activate deactivate deps

VENV_DIRECTORY = venv
AWS_DEFAULT_REGION = ap-northeast-1

virtualenv:
	python3 -m venv $(VENV_DIRECTORY)

clean:
	echo "Try 'rm -fr $(VENV_DIRECTORY)'"

activate:
	echo "Try 'source $(VENV_DIRECTORY)/bin/activate'"

deactivate:
	echo "Try 'deactivate'"

deps:
	pip install -r requirements.txt

deps-diff:
	bash -c "diff -y requirements.lock <(pip freeze)"

format:
	black main.py api remote

serve: check-awsenv1 check-awsenv2
	env FLASK_APP=main.py AWS_DEFAULT_REGION=$(AWS_DEFAULT_REGION) \
	flask run

serve-prod: check-awsenv1 check-awsenv2
	env FLASK_APP=main.py AWS_DEFAULT_REGION=$(AWS_DEFAULT_REGION) \
	flask run --host=0.0.0.0

check-awsenv1:
ifndef AWS_ACCESS_KEY_ID
	$(error AWS_SECRET_ACCESS_KEY is undefined)
endif

check-awsenv2:
ifndef AWS_SECRET_ACCESS_KEY
	$(error AWS_SECRET_ACCESS_KEY is undefined)
endif
