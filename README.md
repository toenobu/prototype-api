## Prerequisite tool
- Python 3.7

## Development Envionment

### Make your virtual envionment
`make virtualenv`

### Activate your virtual environment
`make activate`

### Install dependencies
`make deps`

### format our codes
`make format`

## Serve
You need following environmental variables.

- AWS_ACCESS_KEY_ID=xxxxxxxxxxx
- AWS_SECRET_ACCESS_KEY=xxxxxxxxxxx
- (Option) AWS_DEFAULT_REGION=ap-northeast-1
- (Option) FLASK_APP=main.py
- (Option) FLASK_LOG=WARN

### Server your application
`make serve`

## Main dependencies
- Flask as webframework
  - https://flask.palletsprojects.com/en/1.1.x/
- boto3 as amazon sdk
  - https://boto3.amazonaws.com/v1/documentation/api/latest/index.html
- black as code formatter
  - https://github.com/psf/black