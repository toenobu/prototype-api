# -*- coding: utf-8 -*-
"""
    api.v1.device
"""

from flask import Blueprint, request, escape, jsonify, current_app

from remote.aws import dynamodb

app = current_app

prefix = "/{}/device".format(__package__.replace(".", "/"))
bp = Blueprint("device", __name__, url_prefix=prefix)


@bp.route("/info", methods=("GET",))
def info():
    name = request.args.get("name")

    response = dynamodb.get_time_series_data(table_name=name)
    app.logger.info(response)

    return jsonify(response)
