# -*- coding: utf-8 -*-
"""
    api.v1.preset
"""

from flask import Blueprint, escape, jsonify, current_app

from remote.aws import s3

app = current_app

prefix = "/{}/preset".format(__package__.replace(".", "/"))
bp = Blueprint("preset", __name__, url_prefix=prefix)


@bp.route("/get", methods=("GET",))
def get():
    """
    for vi
    """
    response = s3.get_vi_param()
    app.logger.info(response)

    return jsonify(response)
