# -*- coding: utf-8 -*-
"""
    main
"""

import os, logging

from flask import Flask, escape, request
from flask_cors import CORS

app = Flask(__name__)
# TODO for development
cors = CORS(app, resources={r"/api/v1/*": {"origins": "*"}})

from api.v1 import device, preset

app.register_blueprint(device.bp)
app.register_blueprint(preset.bp)

if os.getenv("FLASK_LOG") in ["INFO", "WARN"]:
    log_level = os.getenv("FLASK_LOG")
    logging.basicConfig(level=log_level)
    logging.getLogger("flask_cors").level = logging._nameToLevel.get(log_level)
    app.logger.info("logger is enabled as {} mode".format(log_level))


@app.route("/")
def hello():
    name = request.args.get("name", "World")
    return f"Hello, {escape(name)}!"
