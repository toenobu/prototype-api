# -*- coding: utf-8 -*-
"""
    remote.aws.s3
"""

import logging
import json

import boto3

logger = logging.getLogger(__name__)

JSON_BUCKET = "prototype-api"
JSON_OBJECT = "prototype-api/params/sample.json"


def get_vi_param() -> str:
    resource = boto3.resource("s3")
    b = resource.Bucket(JSON_BUCKET)
    o = b.Object(JSON_OBJECT)

    response_with_meta = o.get()
    response = get_response_as_string(response_with_meta)

    logger.info("s3 response: {0}".format(response))
    return response


def get_response_as_string(response: dict) -> str:
    """
    Todo
    """

    # Check http status
    if response["ResponseMetadata"]["HTTPStatusCode"] != 200:
        logger.warn(response["ResponseMetadata"])
        logger.warn("something weired")
        return

    payload = response["Body"]
    p = payload.read()

    # flask.jsonify can not convert response
    # without json.dumps(json.loads(response))
    return json.dumps(json.loads(p))
