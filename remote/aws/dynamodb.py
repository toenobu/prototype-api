# -*- coding: utf-8 -*-
"""
    remote.aws.dynamodb
"""

import logging
import json

import boto3

logger = logging.getLogger(__name__)


def get_time_series_data(table_name: str, limit: int = 5) -> str:
    client = boto3.client("dynamodb")

    # make condition
    k_expression = "device_id = :name"
    e_attributeValues = {":name": {"S": table_name}}
    scan_index_forward = False

    response_with_meta = client.query(
        TableName="api-prototype",
        KeyConditionExpression=k_expression,
        ExpressionAttributeValues=e_attributeValues,
        ScanIndexForward=scan_index_forward,
        Limit=limit,
    )

    response = get_response_as_list(response_with_meta)

    logger.info("response: {0}".format(response))
    return response


def get_response_as_list(response: dict) -> list:
    """
    response is mutable data
    """

    # Check http status
    if response["ResponseMetadata"]["HTTPStatusCode"] != 200:
        logger.warn(response["ResponseMetadata"])
        logger.warn("something weired")
        return

    # We may get bytes data
    def convert_to_str(response: dict) -> dict:
        l = response["Items"]
        for x in l:
            if "payload_raw" in x:
                x["payload_raw"] = x["payload_raw"]["B"].decode("utf-8")

    convert_to_str(response)

    return response["Items"]
